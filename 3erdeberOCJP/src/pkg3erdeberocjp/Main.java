/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3erdeberocjp;

/**
 *
 * @author FERNANDO
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        
        //operadores aritméticos        

    int valor = 2 + 5; 
    System.out.println(valor);

    valor = valor - 4; 
    System.out.println(valor);

    valor = valor * 2; 
    System.out.println(valor);

    valor = valor / 2; 
    System.out.println(valor);

    valor += 3; 
    valor = valor % 6; 
    System.out.println(valor);

    String uno = "cadena uno, ";
    String dos = "cadena dos, ";
    System.out.println(uno+dos+"cadena tres.");

    //operadores Unarios
    int valor1 = 4;
    int valor2 = 2;

    valor1--;  
    System.out.println(valor1);

    valor1++;  
    System.out.println(valor1);

    valor1 = -valor1;
    System.out.println(valor1);

    valor2 = --valor1;
    System.out.println(valor2);  // resultado -5
    System.out.println(valor1);  //resultado -5

    valor2 = valor1++;
    System.out.println(valor2); //resultado -5
    System.out.println(valor1); //resultado -4

    boolean falso = false;
    System.out.println(falso); 
    System.out.println(!falso);

    

    // operadores de comparación

    // <, >, >=, <=, ==, !=

    int notaOCJP1= 6;
    int notaOCJP2= 5;
    int notaOCJPtotal = notaOCJP1+ notaOCJP2;

    if(notaOCJP1 >= 7){        
        System.out.println("mas o menos");
    }
    else{
        System.out.println("¡se forzó!");
    }

    if(notaOCJPtotal <14){
        System.out.println("sigue participando");
    }
    else{
        System.out.println("¡que suerte!");
    }

    if (notaOCJPtotal == 14){
        System.out.println("Aprobado");
    }

    if (notaOCJPtotal != 14){
        System.out.println("Suspenso");
    }

    if (notaOCJPtotal <= 11){
        System.out.println("a estudiar!");
    }

    if (notaOCJPtotal > 12){
        System.out.println("suple no tan jodido");
    }
    
    
    //bitwise
      int a = 12;	// 12 = 1100 
      int b = 10;	// 10 = 1010
      int c;

      c = a & b;        // 8 = 1000
      System.out.println("a & b = " + c);

      c = a | b;        // 14 = 1110 
      System.out.println("a | b = " + c );

      c = a ^ b;        // 6 = 0110
      System.out.println("a ^ b = " + c );

      c = ~a;           //-13 = 10011
      System.out.println("~a = " + c );

    // operacion booleana
      
      if(notaOCJP1 <7 && notaOCJP2 <7){
          System.out.println("¡Se jodió!");
      }
      
      if(notaOCJPtotal ==14 || notaOCJPtotal >14){
          System.out.println("¡Aprobado!");
      }
      
      if((notaOCJPtotal <14) ^ (notaOCJPtotal ==14)){
          System.out.println("¡Suple!");
      }
      
      Conversion con = new Conversion();
      
      con.primitivos();
      con.noprimitivos();
      con.clone();
   }
}
