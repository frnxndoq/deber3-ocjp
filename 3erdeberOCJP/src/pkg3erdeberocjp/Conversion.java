package pkg3erdeberocjp;

import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author FERNANDO
 */
public class Conversion extends Vegetales {
    
    // en conversion se debe convertir desde un tipo mas pequeño a un más grande
    
    public void primitivos(){
    
    // por asignación
    int x = 13;
    double y;
    double z = 2;
    double resultado;
    float f;
    long l;
    char c = 2;
    byte b= 23;
    short s =42;
    
    y = x;
    System.out.println("Valor en formato double: "+y);
    f = x;
    System.out.println("Valor en formato float: "+f);
    l = x;
    System.out.println("Valor en formato long: " +l);
    x = c;
    System.out.println("Valor en formato int: " +x);
    x=b;
    System.out.println("Valor en formato int: " +x);
    s= b;
    System.out.println("Valor en formato short: " +s);
    
    //llamado a métodos
    f=5.321f;
    y=Math.cos(f);
    System.out.println("Valor en formato double: " +y);
    
    //promoción aritmética
    resultado = z*x;
    System.out.println("Valor en formato double: "+resultado);
    
    //Casting puede hacer desde un tipo grande a un pequeño
    f= (float) ((x/z)+3);
    
    System.out.println("Valor en formato float: " + f);
    System.out.println("Valor en formato double: " + (double) f);
    System.out.println("Valor en formato int: " + (int) f);
    System.out.println("Valor en formato byte: " + (byte) f);
    System.out.println("Valor en formato short: " + (short) f);
    System.out.println("Valor en formato long: " + (long) f);
    
    }
    
    
    public void noprimitivos(){
        
        //Conversión de referencias a objetos por asignación
        
        //Frutas es una superclase de Conversion
        // tipo clase a tipo clase
        Frutas conv1=new Frutas();
        Conversion conv = conv1;
        
        //tipo clase a tipo interfaz
        manzana apple = conv1;
        
        //tipo interfaz a interfaz
        lechuga lechu = apple;
              
        //arreglos
        Vegetales vegetales[];
        Conversion conversion[] = new Conversion [5];
        
        //funciona porque vegetales es una superclase de Conversion
        vegetales = conversion;
        
        //Conversión por el método
        Vector vect = new Vector();
        vect.add(vegetales);
        
        //Casting de referencia       
        //clases
        Frutas fruits = (Frutas) conv;
        
        //interfaz a clase
        conv = (Conversion) apple;
        //interfaz
        manzana apples=(manzana) lechu;
        
        
        //arreglos
        conversion = (Conversion[]) vegetales;
    }
    
    //Cloneable
    public Object clone(){
        
        Conversion vegetables=null;
        try{
            vegetables=(Conversion)super.clone();
        }catch(CloneNotSupportedException ex){
            System.out.println(" no se puede duplicar");
        }
        vegetables.vegetal=(String[])vegetables.vegetal.clone();
        vegetables.datos();
        System.out.println(vegetables.vegetal[1]+" "+ vegetables.vegetal[0]);
        return vegetables;     
    }
    
   
}
